import React from 'react'
import {isMobile} from 'react-device-detect';
import { Grid } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import Image from './assets/hero-image-8.png'
import './Lobby.css'

function Lobby () {
  return (
    <Grid container className='LobbyContainer' justify='space-between'>
      {isMobile && (
        <Grid item className='LobbyItem'>
          <Alert variant="filled" severity="error" classes={{icon: 'AlertIcon', message: 'AlertMessage'}}>
            Oops. This feature is still not available for mobile devices. Please login from a desktop device to access it.
          </Alert>
        </Grid>
      )}
      <Grid container item direction='row' justify='center' alignItems='center' className='LobbyItem'>
        <Grid item md={8}>
          <Grid container direction='column'>
            <Grid item>
                <span className='Title'>
                  Welcome to our Video Rooms!
                </span>
            </Grid>
            <Grid item>
              <p className='Body1'>The tabs above allow you to connect with the members of our community that have joined each respective video room. Please select your desired room above to join them.</p>
            </Grid>
            <Grid item>
              <p className='Body2'>Your browser may ask for permission to use your camera and microphone. You’ll
                need to click “allow”
                on these prompts to use Video Rooms.
              </p>
            </Grid>
          </Grid>

        </Grid>
        <Grid item md={4}>
          <img src={Image} className='Image' alt="hero"/>
        </Grid>
      </Grid>
      <Grid item className='LobbyItem LobbyFooter'>
        <p className='FootNote'>
          The following video rooms were brought to you with the help of <a href="https://jitsi.org/">Jitsi</a>.

        </p>
      </Grid>
    </Grid>

  )
}

export default Lobby
