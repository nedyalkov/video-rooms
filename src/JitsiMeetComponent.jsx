import React, { useState, useEffect } from 'react';
import ProgressComponent from '@material-ui/core/CircularProgress';
import * as classnames from 'classnames';
import './JitsiMeetComponent.css';

function startConference(roomName, userName, setLoading) {
    try {
        setLoading(true);

        const container = document.querySelector('#jitsi-container');
        if (container && container.lastElementChild) {
            container.removeChild(container.lastElementChild);
        }

        const domain = 'meet.jit.si';
        const options = {
            roomName,
            noSSL: false,
            parentNode: document.getElementById('jitsi-container'),
            interfaceConfigOverwrite: {
                filmStripOnly: false,
                SHOW_JITSI_WATERMARK: false
            },
            configOverwrite: {
                disableSimulcast: false
            }
        };

        // eslint-disable-next-line no-undef
        const currentJitsi = new JitsiMeetExternalAPI(domain, options);
        currentJitsi.addEventListener('videoConferenceJoined', () => {
            console.log('Local User Joined');
            currentJitsi.executeCommand('displayName', userName);
        });
        setLoading(false);
    } catch (error) {
        console.error('Failed to load Jitsi API', error);
    }
}

function JitsiMeetComponent({ roomName, userName }) {
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (window.JitsiMeetExternalAPI) {
            startConference(roomName, userName, setLoading);
        } else {
            alert('Jitsi Meet API script not loaded');
        }
    }, [roomName, userName]);

    return (
        <div className="MeetComponentRoot">
            {loading && <ProgressComponent className="vertical-center" />}
            <div
                id="jitsi-container"
                className={classnames('MeetContainer', { Loading: loading })}
            />
        </div>
    );
}

export default JitsiMeetComponent;
