import React from 'react';
import { withRouter } from 'react-router-dom';
import { AppBar, Tabs, Tab } from '@material-ui/core';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import './App.css';
import JitsiMeetComponent from './JitsiMeetComponent';
import Lobby from './Lobby';

function App(props) {
    const [currentRoom, setCurrentRoom] = React.useState(null);
    const handleRoomChange = (event, newValue) => {
        setCurrentRoom(newValue);
    };
    const paramsString = props.location.search;
    const params = new URLSearchParams(paramsString);
    const roomsString = params.get('rooms') || '';
    const userName = params.get('member') || '';
    const orgPreffix = params.get('preffix') || '';
    // map primaryColor to admin navigation color
    // const primaryColor = params.get('secondaryColor') || 'F3F4FA';
    const primaryColor = 'F3F4FA';
    // map secondaryColor to admin primary color
    // const secondaryColor = params.get('primaryColor') || 'df4b38';
    const secondaryColor = params.get('secondaryColor') || 'df4b38';
    const meetingRooms = roomsString.split(',');
    const theme = createMuiTheme({
        palette: {
            primary: {
                main: `#${primaryColor}`
            },
            secondary: {
                main: `#${secondaryColor}`
            },
            error: {
                main: '#FFD1D4',
            }
        }
    });
    return (
        <MuiThemeProvider theme={theme}>
            <div className="App">
                <AppBar className="header" position="static">
                    <Tabs value={currentRoom} onChange={handleRoomChange}>
                        <Tab key={null} label="Lobby" value={null} />
                        {meetingRooms.map(mr => (
                            <Tab
                                key={mr}
                                label={mr.replace('-', ' ')}
                                value={`${orgPreffix}-${mr}`}
                            />
                        ))}
                    </Tabs>
                </AppBar>

                <div className="content">
                    {currentRoom ? (
                        <JitsiMeetComponent
                            roomName={currentRoom}
                            userName={userName}
                            className="MeetComponent"
                        />
                    ) : (
                        <Lobby />
                    )}
                </div>
            </div>
        </MuiThemeProvider>
    );
}

export default withRouter(App);
